import java.util.List;

public class Main {
    public static <T extends Number> void main(String[] args) {

    }

    //== CASE0
    /*
     * super (lower bound) can't be used with type parameters
     * unexpected token. replace "super" with "extends".
     * */
    static <T super Number> boolean rotateOneElement4(List<T> l){
        return l.add(l.remove(0));
    }

    //== CASE1
    static <T> boolean rotateOneElement(List<T> l){
        return l.add(l.remove(0));
    }
    /*
    * the compiler is not able to deduce that the result of remove is the same type that add expects.
    * */
    static boolean rotateOneElement2(List<?> l){
        return l.add(l.remove(0)); //ERROR!
    }
    /*
    *
     * in the above method declaration(rotateOneElement) you need to specify the type T before the return type.
     * this is the required signature for generic methods with type parameters.
     * but if you create a generic method using wildcard notation, then you don't have to specify
     * any types or wildcards before the return type of the generic method. see rotateOneElement3 method below.
    * */

    /*
    * if you want to have a rotateOneElement method with a wildcard,
    * let the wildcard method call the generic one
    * */
    static void rotateOneElement3(List<?> l){
        rotateOneElement(l);
    }

    //== CASE2
    /*
    * first case enforces both types to be same.
    * While there is no restriction in second case.
    * */
    static <E> void printObjectsExceptOne(List<E> list, E object) {

    }

    static void printObjects(List<?> list, Object object) {

    }
}

//== CASE3

class MyClass<T extends Number>{

}

/*
* unexpected wildcard. can't use wildcards for defining generic classes.
* */
class MyClass2<? extends Number>{

}

//== CASE4

interface MyInterface<T extends Number>{

}

/*
 * unexpected wildcard. can't use wildcards for defining generic interfaces.
 * */
interface MyInterface2<? extends Number>{

}